Source: liblingua-translit-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Kanashiro <kanashiro@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liblingua-translit-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liblingua-translit-perl.git
Homepage: https://metacpan.org/release/Lingua-Translit
Rules-Requires-Root: no

Package: liblingua-translit-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: Perl module that transliterates text between writing systems
 Lingua::Translit can be used to convert text from one writing system to
 another, based on national or international transliteration tables. Where
 possible a reverse transliteration is supported.
 .
 The term transliteration describes the conversion of text from one writing
 system or alphabet to another one. The conversion is ideally unique, mapping
 one character to exactly one character, so the original spelling can be
 reconstructed. Practically this is not always the case and one single letter
 of the original alphabet can be transcribed as two, three or even more
 letters.
 .
 Furthermore there is more than one transliteration scheme for one writing
 system. Therefore it is an important and necessary information, which scheme
 will be or has been used to transliterate a text, to work integrative and be
 able to reconstruct the original data.
